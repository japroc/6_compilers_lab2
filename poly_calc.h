#ifndef POLY_CALC_H
#define POLY_CALC_H
#include "list.h"

/*#define pple struct pc_list_elem *
#define ppl struct pc_list *

#define pl struct pc_list_elem
#define ple struct pc_list*/



int number_pow(int left, int right);

ppl mem_create(char var);
ppl mem_pow(ppl list, int degree);
ppl mem_mul(ppl list, int number);

void print_poly(ppl head);

ppl poly_create(int val);
ppl elem_to_poly(pple elem);
ppl poly_uminus(ppl head);
ppl poly_minus(ppl p1, ppl p2);
ppl poly_plus(ppl p1, ppl p2);
ppl poly_mul(ppl p1, ppl p2);
ppl poly_clear(ppl list);

#endif