%{

#include "poly_calc.h"
#include "poly_lang.h"

#define alloca malloc

%}

%union {
    int val;
    void * pointer;
};

%token PRINT
%token <val> NUMBER END LETTER
%token <pointer> VAR
%type <val> number /*uminus_number*/
%type <pointer> member expr uminus_expr

%left '+' '-'
%left '*' '/'
%left UMINUS
%right '^'
%left ERR

%% 
complete 	: statements END 
				{ 
					printf("\nNice!");
					/*print_poly($1);*/
					printf("\n"); 
					YYACCEPT; 
				}


statements	: statement
			| statements '\n' statement

statement	: 					/* empty lines support */ 
			| VAR '=' expr								
				{ poly_lang_set_var_val($1, $3); }
			| PRINT '(' expr ')'					
				{ poly_lang_print_expr($3); }
			| VAR '=' uminus_expr								
				{ poly_lang_set_var_val($1, $3); }
			| PRINT '(' uminus_expr ')'					
				{ poly_lang_print_expr($3); }

expr	: VAR
			{ $$ = poly_lang_get_var_poly($1); }
		| number 							/* <NUMBER> to prevent 2^2^3, but accept x^4^5. <number> to accept both*/
			{ $$ = poly_create($1); }
		| member
			{ $$ = $1; }

		| '(' expr ')'
			{ $$ = $2; }
		| expr '+' expr			
			{ $$ = poly_lang_var_sum($1, $3); }
		| expr '-' expr			
			{ $$ = poly_lang_var_minus($1, $3); }
		| expr '*' expr			
			{ $$ = poly_lang_var_mul($1, $3); }
		| '(' uminus_expr ')'
			{ $$ = $2; }
		| uminus_expr '+' expr			
			{ $$ = poly_lang_var_sum($1, $3); }
		| uminus_expr '-' expr			
			{ $$ = poly_lang_var_minus($1, $3); }
		| uminus_expr '*' expr			
			{ $$ = poly_lang_var_mul($1, $3); }

		/* ERROR HANDLING */
		| expr '+' '+' expr
			{ poly_lang_err_expr_expected(); }
		| expr '+' '-' expr
			{ poly_lang_err_expr_expected(); }
		| expr '+' '*' expr
			{ poly_lang_err_expr_expected(); }
		| expr '+' '\n' 
			{ poly_lang_err_custom("Expression expected at the end of the line."); }

		| expr '-' '+' expr
			{ poly_lang_err_expr_expected(); }
		| expr '-' '-' expr
			{ poly_lang_err_expr_expected(); }
		| expr '-' '*' expr
			{ poly_lang_err_expr_expected(); }
		| expr '-' '\n' 
			{ poly_lang_err_custom("Expression expected at the end of the line."); }

		| expr '*' '+' expr
			{ poly_lang_err_expr_expected(); }
		| expr '*' '-' expr
			{ poly_lang_err_expr_expected(); }
		| expr '*' '*' expr
			{ poly_lang_err_expr_expected(); }
		| expr '*' '\n'
			{ poly_lang_err_custom("Expression expected at the end of the line."); }

		| uminus_expr '*' '*' expr			
			{ poly_lang_err_expr_expected(); }
		| uminus_expr '*' '-' expr			
			{ poly_lang_err_expr_expected(); }
		| uminus_expr '*' '+' expr			
			{ poly_lang_err_expr_expected(); }
		| uminus_expr '*' '\n'		
			{ poly_lang_err_custom("Expression expected at the end of the line."); }	
		| uminus_expr '+' '*' expr			
			{ poly_lang_err_expr_expected(); }
		| uminus_expr '+' '-' expr			
			{ poly_lang_err_expr_expected(); }
		| uminus_expr '+' '+' expr			
			{ poly_lang_err_expr_expected(); }
		| uminus_expr '+' '\n'		
			{ poly_lang_err_custom("Expression expected at the end of the line."); }
		| uminus_expr '-' '*' expr			
			{ poly_lang_err_expr_expected(); }
		| uminus_expr '-' '-' expr			
			{ poly_lang_err_expr_expected(); }
		| uminus_expr '-' '+' expr			
			{ poly_lang_err_expr_expected(); }
		| uminus_expr '-' '\n'		
			{ poly_lang_err_custom("Expression expected at the end of the line."); }


uminus_expr	: '-' expr 			%prec UMINUS
				{ $$ = poly_uminus($2); }

			/* ERROR CATCHER */ /* debug */
			| '-' uminus_expr
				{ poly_lang_err_custom("Double unary '-' operation."); }
			/*| '*' '-' expr
				{ poly_lang_err_expr_expected(); }
			| '+' '-' expr
				{ poly_lang_err_expr_expected(); }
			| '-' '-' expr
				{ poly_lang_err_expr_expected(); }
			| '^' '-' expr
				{ poly_lang_err_expr_expected(); }*/

member	: LETTER				
		{
			$$ = mem_create($1);
		}
		| LETTER '^' number		%prec '*'
		{
			$$ = mem_create($1);
			$$ = mem_pow($$, $3);
		}
		| NUMBER LETTER					%prec UMINUS
		{
			$$ = mem_create($2);
			$$ = mem_mul($$, $1);
		}
		| NUMBER LETTER '^' number		%prec UMINUS
		{
			$$ = mem_create($2);
			$$ = mem_pow($$, $4);
			$$ = mem_mul($$, $1);
		}

		/* ERROR CATCHER */
		| NUMBER NUMBER
			{ poly_lang_err_custom("Opeartion expected."); }
		| LETTER LETTER
			{ poly_lang_err_custom("Opeartion expected."); }
		| number LETTER				%prec '*'
			{ poly_lang_err_custom("Illegal coefficient"); }
		| number LETTER '^' number	%prec '*'
			{ poly_lang_err_custom("Illegal coefficient"); }

number	: NUMBER
			{ $$ = $1; }
		| number '^' number
			{ $$ = number_pow($1, $3); }
		| '(' '-' number ')'					/* create reduce/reduce conflict */
			{ $$ = (-1) * $3; }					/* but works fine */
