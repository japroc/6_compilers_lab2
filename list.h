#ifndef LIST_H
#define LIST_H

#define pple struct pc_list_elem *
#define ppl struct pc_list *

#define ple struct pc_list_elem
#define pl struct pc_list

struct pc_list
{
	char var;
	struct pc_list_elem * head;
	struct pc_list_elem * tail;
};

struct pc_list_elem
{
	int val;
	int degree;
	pple next;
	pple prev;
};

// create raw list
ppl list_create();
// duplicate list
ppl list_duplicate(ppl list);
// insert elem or replace
pple list_insert_replace(ppl list, pple list_elem);
// find elem by degree
pple list_find_elem_by_degree(ppl list, int deg);
// find elem with lower or eq degree
pple list_find_elem_loweroreq_degree(ppl list, int deg);
// remove elem from list
pple list_remove_elem(pple elem);
// check if list is empty
int list_is_empty(ppl list);

// create elem with specific val and degree
pple elem_create(int val, int degree);
// duplicate elem
pple elem_duplicate(pple elem);

#endif