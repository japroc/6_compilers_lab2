#include "vars_list.h"

#ifndef NULL
#define NULL 0
#endif

pvl vl_create_vars_list()
{
	pvl list = (pvl)malloc(sizeof(vl));
	list->head = (pvle)calloc(sizeof(vle));
	list->tail = (pvle)calloc(sizeof(vle));

	list->head->prev = list->tail->next = NULL;
	list->head->next = list->tail;
	list->tail->prev = list->head;

	return list;
}

pvle vl_create_vars_list_elem(char * _name, ppl _poly)
{
	pvle elem = (pvle)malloc(sizeof(vle));
	elem->next = elem->prev = NULL;

	elem->name = (char *)malloc(strlen(_name) + 1);
	strcpy(elem->name, _name);

	elem->poly = _poly;

	return elem;
}

pvle vl_find_elem_in_list_by_name(pvl list, char * name)
{
	pvle elem = list->head->next;

	while (elem != list->tail)
	{
		if (strcmp(elem->name, name) == 0)
		{
			return elem;
		}

		elem = elem->next;
	}

	return NULL;
}

// debug this
void vl_insert_elem_in_list(pvl list, pvle elem)
{
	elem->prev = list->head;
	elem->next = list->head->next;
	list->head->next->prev = elem;
	list->head->next = elem;
}