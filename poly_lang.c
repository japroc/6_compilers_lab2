#include "poly_lang.h"
#include "poly_calc.h"
#include <stdlib.h> // exit

#ifndef NULL
#define NULL 0
#endif

extern int yychar;
int yyerror(char * str);
pvl gloval_vars_list;

void poly_lang_init()
{
	gloval_vars_list = vl_create_vars_list();
}

// --------
// printers
// --------

void poly_lang_print_var(char * name)
{
	pvle elem;

	elem = vl_find_elem_in_list_by_name(gloval_vars_list, name);
	if (elem)
		print_poly(elem->poly);
	else
	{
		yyerror("Semantic error : Unable to print variable that does not exist.");
		exit(1);
	}
}

void poly_lang_print_expr(ppl poly)
{
	print_poly(poly);
}

// ----------
// addignment
// ----------

void poly_lang_set_var_val(char * name, ppl poly)
{
	pvle elem;

	elem = vl_find_elem_in_list_by_name(gloval_vars_list, name);
	if (elem)
		elem->poly = poly;
	else
	{
		elem = vl_create_vars_list_elem(name, poly);
		vl_insert_elem_in_list(gloval_vars_list, elem);
	}
}

// ----------
// operations
// ----------

ppl poly_lang_get_var_poly(char *name)
{
	pvle elem;

	elem = vl_find_elem_in_list_by_name(gloval_vars_list, name);
	if (elem)
		return elem->poly;
	else
	{
		yychar = 0;
		yyerror("Semantic error : Illegal operation. Variable was not described.");
		exit(1);
	}

	return NULL;
}

ppl poly_lang_var_mul(ppl left, ppl right)
{
	return poly_mul(left, right);
}

ppl poly_lang_var_sum(ppl left, ppl right)
{
	return poly_plus(left, right);
}

ppl poly_lang_var_minus(ppl left, ppl right)
{
	return poly_minus(left, right);
}

// ------
// ERRORS
// ------

void poly_lang_err_expr_expected()
{
	yychar = 0;
	yyerror("Syntax Error : Expression expected.");
	exit(1);
}

void poly_lang_err_custom(char * str)
{
	yychar = 0;
	yyerror(str);
	exit(1);
}
