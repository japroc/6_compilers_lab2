#include "poly_calc.h"
#include <stdio.h>
#include <math.h>

extern int yychar;

int check_vars(ppl left, ppl right);

int check_vars(ppl left, ppl right)
{
	if (!left->var || !right->var)
		return 1;

	return (left->var == right->var);
}

// ---------------------
// number implementation
// ---------------------

int number_pow(int left, int right)
{
	return pow(left, right);
}

// ---------------------
// member implementation
// ---------------------

ppl mem_create(char var)
{
	ppl list = list_create();
	pple elem = elem_create(1, 1);

	list->var = var;
	list_insert_replace(list, elem);

	return list;
}

ppl mem_pow(struct pc_list * list, int degree)
{	
	list->head->next->degree *= degree;

	return list;
}

ppl mem_mul(ppl list, int number)
{
	list->head->next->val *= number;

	return list;
}

// -------------------
// poly implementation
// -------------------

void print_poly(ppl list)
{
	// check if poly = 0
	if (list_is_empty(list))
	{
		printf("0\n");
		return;
	}

	pple elem = list->head->next;

	for (pple elem = list->head->next; elem != list->tail; elem = elem->next)
	{
		if (elem->degree != 0)
		{
			// print coeff
			if (elem->val >= 0 && elem != list->head->next)
				printf("+");

			if (elem->val == -1)
				printf("-");
			else if (elem->val == 1);
			else
				printf("%d", elem->val);

			// print var
			printf("%c", list->var);

			// print degree
			if (elem->degree == 1);
			else
			{
				printf("^");
				if (elem->degree > 1)
					printf("%d", elem->degree);
				else
					printf("(%d)", elem->degree);
			}
		}
		else
		{
			if (elem->val >= 0 && elem != list->head->next)
				printf("+");
			
			printf("%d", elem->val);
		}
	}

	printf("\n");
}

ppl poly_uminus(ppl list)
{
	for (pple tmp = list->head->next; tmp != list->tail; tmp=tmp->next)
		tmp->val *= (-1);

	return list;
}

ppl poly_create(int val)
{
	ppl list = list_create();
	pple elem = elem_create(val, 0);

	list->var = NULL;
	list_insert_replace(list, elem);

	return list;
}

ppl poly_minus(ppl p1, ppl p2)
{
	if (!check_vars(p1, p2))
	{
		yychar = 0;
		yyerror("Semantic error : Illegal operation for polynomails with different variables");
		exit(1);
	}

	ppl result = list_duplicate(p1);
	result->var = (p1->var) ? p1->var : p2->var;

	for( pple p2_tmp = p2->head->next; p2_tmp != p2->tail; p2_tmp = p2_tmp->next)
	{
		pple elem = list_find_elem_by_degree(result, p2_tmp->degree);

		// if found
		if (elem)
		{
			elem->val -= p2_tmp->val;
			continue;
		}

		// if not found
		pple new_elem = elem_duplicate(p2_tmp);
		new_elem->val *= (-1);
		elem = list_insert_replace(result, new_elem);
		//elem->val *= (-1);
	}

	result = poly_clear(result);
	return result;
}

ppl poly_mul(ppl p1, ppl p2)
{
	if (!check_vars(p1, p2))
	{
		yychar = 0;
		yyerror("Semantic error : Illegal operation for polynomails with different variables");
		exit(1);
	}

	ppl result = list_create();
	result->var = (p1->var) ? p1->var : p2->var;

	pple tmp = p2->head->next;
	while (tmp != p2->tail)
	{
		pple p1_tmp = p1->head->next;

		while (p1_tmp != p1->tail)
		{
			pple new_elem = elem_create(p1_tmp->val * tmp->val, p1_tmp->degree + tmp->degree);
			ppl new_poly = elem_to_poly(new_elem);
			new_poly->var = p1->var;

			result = poly_plus(result, new_poly);
			p1_tmp = p1_tmp->next;
		}

		tmp = tmp->next;
	}

	return result;
}

ppl poly_plus(ppl p1, ppl p2)
{
	if (!check_vars(p1, p2))
	{
		yychar = 0;
		yyerror("Semantic error : Illegal operation for polynomails with different variables");
		exit(1);
	}

	ppl result = list_duplicate(p1);
	result->var = (p1->var) ? p1->var : p2->var;

	for (pple p2_tmp = p2->head->next; p2_tmp != p2->tail; p2_tmp = p2_tmp->next)
	{
		pple elem = list_find_elem_by_degree(result, p2_tmp->degree);

		// if found
		if (elem)
		{
			elem->val += p2_tmp->val;
			continue;
		}

		// if not found
		pple new_elem = elem_duplicate(p2_tmp);
		elem = list_insert_replace(result, new_elem);
	}

	result = poly_clear(result);
	return result;
}

ppl poly_clear(ppl list)
{
	for (pple elem = list->head->next; elem != list->tail; elem = elem->next)
	{
		if (elem->val == 0)
			elem = list_remove_elem(elem);
	}

	return list;
}

ppl elem_to_poly(pple elem)
{
	ppl list = list_create();

	list_insert_replace(list, elem);

	return list;
}