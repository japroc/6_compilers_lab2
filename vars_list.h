#ifndef LIST_VARS_H
#define LIST_VARS_H

#include "list.h"

#define pvl struct vars_list *
#define pvle struct vars_list_elem *

#define vl struct vars_list
#define vle struct vars_list_elem

struct vars_list
{
	struct vars_list_elem * head;
	struct vars_list_elem * tail;
};

struct vars_list_elem
{
	char * name;
	ppl poly;
	
	pvle next;
	pvle prev;
};

pvl vl_create_vars_list();
pvle vl_create_vars_list_elem(char * _name, ppl _poly);
pvle vl_find_elem_in_list_by_name(pvl list, char * name);
void vl_insert_elem_in_list(pvl list, pvle elem);

#endif