#ifndef POLY_LANG_H
#define POLY_LANG_H

#include "vars_list.h"

void poly_lang_init();

// --------
// printers
void poly_lang_print_var(char * name); /* debug */
void poly_lang_print_expr(ppl poly);

// ----------
// addignment
void poly_lang_set_var_val(char * name, ppl poly);

// ----------
// operations
ppl poly_lang_get_var_poly(char *name);
ppl poly_lang_var_sum(ppl left, ppl right);
ppl poly_lang_var_minus(ppl left, ppl right);
ppl poly_lang_var_mul(ppl left, ppl right);

// -----
// ERROR

void poly_lang_err_expr_expected();
void poly_lang_err_custom(char * str);

#endif