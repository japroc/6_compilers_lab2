#include "list.h"

#define NULL 0


// -------------------
// LIST IMPLEMENTATION
// -------------------

ppl list_create()
{
	ppl new_list = (ppl)malloc(sizeof(pl));
	new_list->head = (pple)malloc(sizeof(ple));
	new_list->tail = (pple)malloc(sizeof(ple));

	new_list->head->degree = new_list->head->val = 0;
	new_list->tail->degree = new_list->tail->val = 0;

	new_list->head->prev = new_list->tail->next = NULL;
	new_list->head->next = new_list->tail;
	new_list->tail->prev = new_list->head;

	return new_list;
}

/* debug */
ppl list_duplicate(ppl list)
{
	ppl dupl_list = list_create();
	dupl_list->var = list->var;

	for (pple elem = list->head->next; elem != list->tail; elem = elem->next)
	{
		pple dupl_elem = elem_duplicate(elem);
		list_insert_replace(dupl_list, dupl_elem);
	}

	return dupl_list;
}

pple list_insert_replace(ppl list, pple list_elem)
{
	pple tmp = list->head->next;
	while (tmp != list->tail && tmp->degree > list_elem->degree)
	{
		tmp = tmp->next;
	}

	if (tmp != list->tail && tmp->degree == list_elem->degree)
	{
		tmp->val = list_elem->val;
	}
	else //(tmp->degree < list_elem->degree)
	{
		list_elem->prev = tmp->prev;
		list_elem->next = tmp;

		tmp->prev->next = list_elem;
		tmp->prev = list_elem;
	}

	return list_elem;
}

pple list_find_elem_by_degree(ppl list, int deg)
{
	pple elem = list->head->next;
	while (elem != list->tail)
	{
		if (elem->degree == deg) return elem;
		elem = elem->next;
	}

	return NULL;
}

pple list_find_elem_loweroreq_degree(ppl list, int deg)
{
	pple elem = list->head->next;
	while (elem != list->tail)
	{
		if (elem->degree <= deg) return elem;
		elem = elem->next;
	}

	return NULL;
}

pple list_remove_elem(pple elem)
{
	pple tmp = elem->prev;

	elem->next->prev = elem->prev;
	elem->prev->next = elem->next;

	//free(elem);

	return tmp;
}

int list_is_empty(ppl list)
{
	return list->head->next == list->tail;
}

// ------------------------
// LIST_ELEM IMPLEMENTATION
// ------------------------

pple elem_create(int val, int degree)
{
	pple elem = (pple)malloc(sizeof(ple));
	
	elem->val = val;
	elem->degree = degree;
	elem->next = elem->prev = NULL;

	return elem;
}

pple elem_duplicate(pple elem)
{
	return elem_create(elem->val, elem->degree);
}
