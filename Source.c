/*
Error handlers:
	1. --y					Double unary '-' operation
	2. -y--2				Syntax Error : Expression expected.
	3. y-2-					Expression expected at the end of the line
	4. +y-2	| +-y | +-y		Illegal operaion at the start of the expr.
	5. 2 2x	| xx			Operation expected.
	6. (2+2x))				Syntax error : Illegal bracket character.
	7. ((x+1) | (x+1))		Bracket error.
	8. x + y				Semantic error : Illegal operation for polynomails with different variables.
	9.						Semantic error : Illegal operation. Variable was not described.
	10. $$A = ...			Lexical error : Incorrect variable name.
	11.	$asdfsrgsrtgjaeirlbhixlfjlA = x + y		Lexical error : Incorrect variable name. Var name is too long.
	12. $A = Y + 2			Use lower case symbols for polynomial.
*/



#include "y_tab.h"
#include <stdio.h>
#include <stdlib.h>	// calloc
#include <string.h> // strlen
#include <ctype.h>	// isdigit
#include "poly_lang.h" // poly_lang_init

int yyparse(void);

FILE * fp;
unsigned int line = 1;
extern int yychar;
char c = 0;
int bracket_counter = 0;

int is_print();
void fungetc(char c);
char * var_name_handler();
int is_var_name_char(char c);
int is_lower_case(char c);
int is_higher_case(char c);
int get_number(int val);
void skip_comment();
void end_line_handler();

int main()
{
	poly_lang_init();
	fp = fopen("code.txt", "rb");
	yyparse();
	fclose(fp);
	
	return 0;
}

int yylex()
{
label_read:
	if (c == '\n')
		line++;

	c = fgetc(fp);

	// if skipp symbol jmp to read next
	if (is_skip_symbol(c))
		goto label_read;

	// number handler
	if (isdigit(c))
	{
		yylval.val = get_number(c - '0');
		return NUMBER;
	}

	// print function
	if (c == 'p' && is_print())
	{
		return PRINT;
	}
	// lower case char
	if (is_lower_case(c))
	{
		yylval.val = c;
		return LETTER;
	}
	
	if (is_higher_case(c))
	{
		poly_lang_err_custom("Use lower case symbols for polynomial.");
	}

	switch (c)
	{
	case EOF:
		return END;
	case '$':
		yylval.pointer = var_name_handler();
		return VAR;
	case '#':
		skip_comment();
		goto label_read;
	case '(':
		bracket_counter++;
		return c;
	case ')':
		bracket_counter--;
		return c;
	case '\n':
		end_line_handler();
		return c;
	default:
		return c;
	}
}

int yyerror(char * str)
{
	// char cd = yychar;

	switch (yychar)
	{
	case '-':
		str = "Double unary '-' operation";
		break;
	case '+':
	case '*':
		str = "Syntax error : Illegal operaion at the start of the expr.";
		break;
	default:
		break;
	}

	printf("line %u : %s\n", line, str);

	return 0;
}

// --------------
// Help Functions
// --------------

int is_higher_case(char c)
{
	return c >= 'A' && c <= 'Z';
}

void end_line_handler()
{
	if (bracket_counter != 0)
	{
		poly_lang_err_custom("Bracket error.");
	}
}

int is_skip_symbol(char c)
{
	switch (c)
	{
	case ' ':
	case '\r':
	case '\t':
		return 1;
	default:
		return 0;
	}
}

int is_print()
{
	char arr[4];

	arr[0] = fgetc(fp);
	if (arr[0] != 'r')
	{
		fungetc(arr[0]);
		return 0;
	}

	arr[1] = fgetc(fp);
	if (arr[1] != 'i')
	{
		fungetc(arr[1]);
		fungetc(arr[0]);
		return 0;
	}

	arr[2] = fgetc(fp);
	if (arr[2] != 'n')
	{
		fungetc(arr[2]);
		fungetc(arr[1]);
		fungetc(arr[0]);
		return 0;
	}

	arr[3] = fgetc(fp);
	if (arr[3] != 't')
	{
		fungetc(arr[3]);
		fungetc(arr[2]);
		fungetc(arr[1]);
		fungetc(arr[0]);
		return 0;
	}

	return 1;
}

void fungetc(char c)
{
	if (c != EOF)
		fseek(fp, -1, SEEK_CUR);
	else
		fseek(fp, 0, SEEK_CUR);
}

char * var_name_handler()
{
	char c;
	char * name = (char *)calloc(10, 1);

	while (1)
	{
		c = fgetc(fp);

		if (is_var_name_char(c))
		{
			int idx = strlen(name);
			if (idx < 9)
			{
				name[strlen(name)] = c;
			}
			else
			{
				yyerror("Lexical error : Incorrect variable name. Var name is too long.");
				exit(1);
			}
		}
		else
		{
			fungetc(c);
			break;
		}
	}

	if (strlen(name) == 0)
	{
		yyerror("Lexical error : Incorrect variable name.");
		exit(1);
	}
	return name;
}

int is_var_name_char(char c)
{
	// a...z
	// A...Z
	// 0...9
	// and '_'

	if (is_lower_case(c))
		return 1;
	if (isdigit(c))
		return 1;
	if (c >= 'A' && c <= 'Z')
		return 1;
	if (c == '_')
		return 1;

	return 0;
}

int is_lower_case(char c)
{
	return (c >= 'a' && c <= 'z');
}

int get_number(int val)
{
	char c;
	int tmp;

	while (1)
	{
		c = fgetc(fp);
		if (isdigit(c))
		{
			val = val * 10 + (c - '0');
		}
		else if (c != EOF)
		{
			fseek(fp, -1, SEEK_CUR);
			break;
		}
		else
		{
			fseek(fp, 0, SEEK_CUR);
			break;
		}
	}

	tmp = val;
	return tmp;
}

void skip_comment()
{
	char c;

	while (1)
	{
		c = fgetc(fp);
		if (c == '\n')
		{
			fseek(fp, -1, SEEK_CUR);
			break;
		}
		else if (c == EOF)
		{
			fseek(fp, 0, SEEK_CUR);
			break;
		}
	}

	return;
}